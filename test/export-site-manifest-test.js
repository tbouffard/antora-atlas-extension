/* eslint-env mocha */
'use strict'

const { ajv, expect } = require('./harness')
const ContentCatalog = require('@antora/content-classifier/content-catalog')
const exportSiteManifest = require('@antora/atlas-extension/export-site-manifest')
const schema = require('@antora/atlas-extension/schema')

const validate = ajv.compile(schema)

describe('exportSiteManifest', () => {
  it('should create virtual site manifest file with trailing newline', () => {
    const contentCatalog = new ContentCatalog()
    contentCatalog.registerComponentVersion('the-component', '2.0')
    contentCatalog.addFile({
      src: { component: 'the-component', version: '2.0', module: 'ROOT', family: 'page', relative: 'the-page.adoc' },
    })
    const actual = exportSiteManifest(contentCatalog, 'https://docs.example.org')
    expect(actual).to.exist()
    expect(actual).to.have.property('contents')
    expect(actual.contents).to.be.instanceOf(Buffer)
    expect(actual).to.deep.include({
      mediaType: 'application/json',
      out: { path: 'site-manifest.json' },
      path: 'site-manifest.json',
      pub: { url: '/site-manifest.json', rootPath: '' },
    })
    expect(actual.contents.lastIndexOf('\n')).to.equal(actual.contents.length - 1)
    const actualData = JSON.parse(actual.contents)
    expect(actualData.url).to.equal('https://docs.example.org')
    expect(actualData).to.have.property('components')
    expect(validate(actualData)).to.be.true()
  })

  it('should format manifest data to be human-readable', () => {
    const contentCatalog = new ContentCatalog()
    contentCatalog.registerComponentVersion('the-component', '2.0')
    contentCatalog.addFile({
      src: { component: 'the-component', version: '2.0', module: 'ROOT', family: 'page', relative: 'the-page.adoc' },
    })
    const actual = exportSiteManifest(contentCatalog, 'https://docs.example.org').contents.toString()
    expect(actual).to.startWith('{\n')
    expect(actual).to.include('\n  "url": "https://docs.example.org",\n')
    expect(actual).to.include('\n  "components": {\n')
  })

  it('should not set url on manifest if not set on site', () => {
    const contentCatalog = new ContentCatalog()
    contentCatalog.registerComponentVersion('the-component', '2.0')
    const actual = exportSiteManifest(contentCatalog)
    const actualData = JSON.parse(actual.contents)
    expect(actualData).to.not.have.property('url')
    expect(validate(actualData)).to.be.true()
  })

  it('should set creation metadata on manifest', () => {
    const contentCatalog = new ContentCatalog()
    contentCatalog.registerComponentVersion('the-component', '2.0')
    contentCatalog.addFile({
      src: { component: 'the-component', version: '2.0', module: 'ROOT', family: 'page', relative: 'the-page.adoc' },
    })
    const actual = exportSiteManifest(contentCatalog, 'https://docs.example.org')
    const actualData = JSON.parse(actual.contents)
    expect(actualData.$id).to.equal(schema.$id)
    expect(actualData.generated).to.match(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/)
    expect(validate(actualData)).to.be.true()
  })

  it('should allow manifest file path to be specified', () => {
    const contentCatalog = new ContentCatalog()
    contentCatalog.registerComponentVersion('the-component', '2.0')
    contentCatalog.addFile({
      src: { component: 'the-component', version: '2.0', module: 'ROOT', family: 'page', relative: 'the-page.adoc' },
    })
    const actual = exportSiteManifest(contentCatalog, 'https://docs.example.org', 'manifest.json')
    expect(actual.path).to.equal('manifest.json')
    expect(actual.pub.url).to.equal('/manifest.json')
  })

  it('should alphabetize components in manifest by name property', () => {
    const contentCatalog = new ContentCatalog()
    const nums = { a: 4, b: 3, c: 2, d: 1 }
    ;['d', 'a', 'c', 'b'].forEach((name) => {
      contentCatalog.registerComponentVersion(name, '1.0', { title: `${nums[name]} - ${name.toUpperCase()}` })
    })
    contentCatalog.getComponents().forEach(({ name }) => {
      contentCatalog.addFile({
        src: { component: name, version: '1.0', module: 'ROOT', family: 'page', relative: 'the-page.adoc' },
      })
    })
    const actual = exportSiteManifest(contentCatalog, 'https://docs.example.org')
    const actualData = JSON.parse(actual.contents)
    expect(Object.keys(actualData.components)).to.eql(['a', 'b', 'c', 'd'])
    expect(Object.values(actualData.components).map(({ title }) => title)).to.eql(['4 - A', '3 - B', '2 - C', '1 - D'])
    expect(validate(actualData)).to.be.true()
  })

  it('should not add entry for component with no pages', () => {
    const contentCatalog = new ContentCatalog()
    contentCatalog.registerComponentVersion('the-component', '2.0', { startPage: true })
    const actual = exportSiteManifest(contentCatalog, 'https://docs.example.org')
    const actualData = JSON.parse(actual.contents)
    expect(actualData.components).to.eql({})
    expect(validate(actualData)).to.be.true()
  })

  it('should not add entry for component that was imported from another site', () => {
    const primarySite = { url: 'https://docs.example.org' }
    const contentCatalog = new ContentCatalog()
    contentCatalog.registerComponentVersion('the-component', '2.0', { startPage: true })
    contentCatalog.addFile({
      src: { component: 'the-component', version: '2.0', module: 'ROOT', family: 'page', relative: 'index.adoc' },
    })
    contentCatalog.registerComponentVersion('imported', '1.0').site = primarySite
    contentCatalog.addFile({
      src: { component: 'imported', version: '1.0', module: 'ROOT', family: 'page', relative: 'the-page.adoc' },
    })
    contentCatalog.getComponent('imported').site = primarySite
    const actual = exportSiteManifest(contentCatalog, primarySite.url)
    const actualData = JSON.parse(actual.contents)
    expect(Object.keys(actualData.components)).to.have.lengthOf(1)
    expect(actualData.components).to.have.property('the-component')
    expect(validate(actualData)).to.be.true()
  })

  it('should not add entry for component version with no pages', () => {
    const contentCatalog = new ContentCatalog()
    contentCatalog.registerComponentVersion('the-component', '1.0', { startPage: true })
    contentCatalog.registerComponentVersion('the-component', '2.0', { startPage: true })
    contentCatalog.addFile({
      src: { component: 'the-component', version: '2.0', module: 'ROOT', family: 'page', relative: 'the-page.adoc' },
    })
    const actual = exportSiteManifest(contentCatalog, 'https://docs.example.org')
    const actualData = JSON.parse(actual.contents)
    expect(Object.keys(actualData.components['the-component'].versions)).to.have.lengthOf(1)
    expect(Object.keys(actualData.components['the-component'].versions)[0]).to.equal('2.0')
    expect(actualData.components['the-component'].versions['2.0'].pages).to.have.lengthOf(1)
    expect(validate(actualData)).to.be.true()
  })

  // NOTE code assumes that versions in content catalog are sorted
  it('should add entry for each component version that has pages', () => {
    const contentCatalog = new ContentCatalog()
    contentCatalog.registerComponentVersion('the-component', '2.0')
    contentCatalog.registerComponentVersion('the-component', '1.0')
    contentCatalog.addFile({
      src: { component: 'the-component', version: '1.0', module: 'ROOT', family: 'page', relative: 'index.adoc' },
    })
    contentCatalog.addFile({
      src: { component: 'the-component', version: '2.0', module: 'ROOT', family: 'page', relative: 'index.adoc' },
    })
    contentCatalog.registerComponentVersionStartPage('the-component', '2.0')
    contentCatalog.registerComponentVersionStartPage('the-component', '1.0')
    const actual = exportSiteManifest(contentCatalog, 'https://docs.example.org')
    const actualData = JSON.parse(actual.contents)
    expect(Object.keys(actualData.components)).to.have.lengthOf(1)
    expect(actualData.components['the-component'].latest).to.equal('2.0')
    const versions = actualData.components['the-component'].versions
    expect(Object.values(versions)).to.have.lengthOf(2)
    expect(Object.keys(versions)[0]).to.equal('2.0')
    expect(versions['2.0'].url).to.equal('/the-component/2.0/index.html')
    expect(versions['2.0']).to.not.have.property('prerelease')
    expect(versions['2.0']).to.not.have.property('title')
    expect(Object.keys(versions)[1]).to.equal('1.0')
    expect(versions['1.0']).to.not.have.property('prerelease')
    expect(versions['1.0']).to.not.have.property('title')
    expect(versions['1.0'].url).to.equal('/the-component/1.0/index.html')
    expect(validate(actualData)).to.be.true()
  })

  it('should not add entry for component version that was imported from another site', () => {
    const contentCatalog = new ContentCatalog()
    contentCatalog.registerComponentVersion('the-component', '2.0').site = { url: 'https://docs.example.org' }
    contentCatalog.registerComponentVersion('the-component', '1.0')
    contentCatalog.addFile({
      src: { component: 'the-component', version: '1.0', module: 'ROOT', family: 'page', relative: 'index.adoc' },
    })
    contentCatalog.addFile({
      src: { component: 'the-component', version: '2.0', module: 'ROOT', family: 'page', relative: 'index.adoc' },
    })
    contentCatalog.registerComponentVersionStartPage('the-component', '2.0')
    contentCatalog.registerComponentVersionStartPage('the-component', '1.0')
    const actual = exportSiteManifest(contentCatalog, 'https://docs-archive.example.org')
    const actualData = JSON.parse(actual.contents)
    expect(Object.keys(actualData.components)).to.have.lengthOf(1)
    expect(Object.keys(actualData.components['the-component'].versions)).to.have.lengthOf(1)
    // Q: should latest be the latest in the current site instead?!?
    expect(actualData.components['the-component'].latest).to.equal('2.0')
    expect(Object.keys(actualData.components['the-component'].versions)[0]).to.equal('1.0')
    expect(validate(actualData)).to.be.true()
  })

  it('should add entry for unversioned component version', () => {
    const contentCatalog = new ContentCatalog()
    contentCatalog.registerComponentVersion('unversioned-component', '', { startPage: true, title: 'The Component' })
    contentCatalog.addFile({
      src: { component: 'unversioned-component', version: '', module: 'ROOT', family: 'page', relative: 'index.adoc' },
    })
    const actual = exportSiteManifest(contentCatalog, 'https://docs-archive.example.org')
    const actualData = JSON.parse(actual.contents)
    expect(Object.keys(actualData.components)).to.have.lengthOf(1)
    const component = actualData.components['unversioned-component']
    expect(component.title).to.equal('The Component')
    expect(component.latest).to.equal('')
    expect(Object.keys(component.versions)).to.have.lengthOf(1)
    expect(Object.keys(component.versions)[0]).to.equal('')
    expect(component.versions['']).to.not.have.property('displayVersion')
    expect(component.versions[''].url).to.equal('/unversioned-component/index.html')
    expect(validate(actualData)).to.be.true()
  })

  it('should add entry for unversioned component version of ROOT component', () => {
    const contentCatalog = new ContentCatalog()
    contentCatalog.registerComponentVersion('ROOT', '', { startPage: true, title: 'Home' })
    contentCatalog.addFile({
      src: { component: 'ROOT', version: '', module: 'ROOT', family: 'page', relative: 'index.adoc' },
    })
    const actual = exportSiteManifest(contentCatalog, 'https://docs-archive.example.org')
    const actualData = JSON.parse(actual.contents)
    expect(Object.keys(actualData.components)).to.have.lengthOf(1)
    const component = actualData.components.ROOT
    expect(component.title).to.equal('Home')
    expect(Object.keys(component.versions)).to.have.lengthOf(1)
    expect(Object.keys(component.versions)[0]).to.equal('')
    expect(component.versions[''].url).to.equal('/index.html')
    expect(validate(actualData)).to.be.true()
  })

  it('should set prerelease to true if component version is a prerelease', () => {
    const contentCatalog = new ContentCatalog()
    contentCatalog.registerComponentVersion('new-project', '1.0.0', {
      startPage: true,
      title: 'New Project',
      prerelease: '-SNAPSHOT',
    })
    contentCatalog.addFile({
      src: { component: 'new-project', version: '1.0.0', module: 'ROOT', family: 'page', relative: 'index.adoc' },
    })
    const actual = exportSiteManifest(contentCatalog, 'https://docs.example.org')
    const actualData = JSON.parse(actual.contents)
    expect(Object.keys(actualData.components)).to.have.lengthOf(1)
    const component = actualData.components['new-project']
    expect(Object.keys(component.versions)).to.have.lengthOf(1)
    expect(Object.keys(component.versions)[0]).to.equal('1.0.0')
    expect(component.versions['1.0.0'].displayVersion).to.equal('1.0.0-SNAPSHOT')
    expect(component.versions['1.0.0'].prerelease).to.be.true()
    expect(validate(actualData)).to.be.true()
  })

  it('should set url on exported component version to url of start page', () => {
    const contentCatalog = new ContentCatalog()
    contentCatalog.registerComponentVersion('the-component', '1.0')
    // NOTE it should skip past this file
    contentCatalog.addFile({
      src: {
        component: 'the-component',
        version: '1.0',
        module: 'ROOT',
        family: 'page',
        relative: 'about.adoc',
      },
    })
    const startPage = contentCatalog.addFile({
      src: {
        component: 'the-component',
        version: '1.0',
        module: 'ROOT',
        family: 'page',
        relative: 'the-start-page.adoc',
      },
    })
    contentCatalog.registerComponentVersionStartPage('the-component', '1.0', startPage.src.relative)
    const actual = exportSiteManifest(contentCatalog, 'https://docs.example.org')
    const actualData = JSON.parse(actual.contents)
    const componentVersionInManifest = actualData.components['the-component'].versions['1.0']
    expect(componentVersionInManifest.url).to.equal(startPage.pub.url)
    const pagesInManifest = componentVersionInManifest.pages
    const startPageAliasInManifest = pagesInManifest.find((it) => it.path === 'index.adoc' && !it.module)
    expect(startPageAliasInManifest.alias).to.eql({
      ref: 'the-start-page.adoc',
      url: '/the-component/1.0/the-start-page.html',
    })
    expect(startPageAliasInManifest.url).to.equal('/the-component/1.0/index.html')
    const startPageInManifest = pagesInManifest.find((it) => it.path === 'the-start-page.adoc' && !it.module)
    expect(startPageInManifest).to.not.have.property('alias')
    expect(startPageInManifest.url).to.equal(startPage.pub.url)
    expect(validate(actualData)).to.be.true()
  })

  it('should set title on component version entry if it does not match version', () => {
    const contentCatalog = new ContentCatalog()
    contentCatalog.registerComponentVersion('the-component', '1.0', { displayVersion: '1_0' })
    contentCatalog.addFile({
      src: { component: 'the-component', version: '1.0', module: 'ROOT', family: 'page', relative: 'index.adoc' },
    })
    const actual = exportSiteManifest(contentCatalog, 'https://docs.example.org')
    const actualData = JSON.parse(actual.contents)
    expect(Object.keys(actualData.components['the-component'].versions)[0]).to.equal('1.0')
    expect(actualData.components['the-component'].versions['1.0']).to.have.property('displayVersion', '1_0')
    expect(validate(actualData)).to.be.true()
  })

  it('should add entry for page in component version entry', () => {
    const contentCatalog = new ContentCatalog()
    contentCatalog.registerComponentVersion('the-component', '2.0')
    contentCatalog.addFile({
      src: { component: 'the-component', version: '2.0', module: 'ROOT', family: 'page', relative: 'the-page.adoc' },
      title: 'The Page',
    })
    contentCatalog.addFile({
      src: { component: 'the-component', version: '2.0', module: 'b', family: 'page', relative: 'index.adoc' },
      title: 'Page B',
      asciidoc: { xreftext: 'B' },
    })
    contentCatalog.registerComponentVersionStartPage('the-component', '2.0', 'the-page.adoc')
    const actual = exportSiteManifest(contentCatalog, 'https://docs.example.org')
    const actualData = JSON.parse(actual.contents)
    const pages = actualData.components['the-component'].versions['2.0'].pages.filter((it) => !it.alias)
    expect(pages).to.have.lengthOf(2)
    const thePage = pages.find((it) => it.path === 'the-page.adoc')
    expect(thePage).to.eql({
      path: 'the-page.adoc',
      title: 'The Page',
      url: '/the-component/2.0/the-page.html',
    })
    const bPage = pages.find((it) => it.path === 'index.adoc')
    expect(bPage).to.eql({
      path: 'index.adoc',
      module: 'b',
      url: '/the-component/2.0/b/index.html',
      title: 'Page B',
      xreftext: 'B',
    })
    expect(validate(actualData)).to.be.true()
  })

  it('should use URLs in accordance with HTML URL extension style', () => {
    const contentCatalog = new ContentCatalog({ urls: { htmlExtensionStyle: 'indexify' } })
    contentCatalog.registerComponentVersion('the-component', '2.0', { title: 'The Component' })
    contentCatalog.addFile({
      src: { component: 'the-component', version: '2.0', module: 'ROOT', family: 'page', relative: 'index.adoc' },
      title: 'Index Page',
    })
    contentCatalog.addFile({
      src: { component: 'the-component', version: '2.0', module: 'ROOT', family: 'page', relative: 'the-page.adoc' },
      title: 'The Page',
    })
    contentCatalog.registerComponentVersionStartPage('the-component', '2.0')
    const actual = exportSiteManifest(contentCatalog, 'https://docs.example.org')
    const actualData = JSON.parse(actual.contents)
    const version = actualData.components['the-component'].versions['2.0']
    expect(version.url).to.equal('/the-component/2.0/')
    expect(version.pages.find((it) => it.path === 'index.adoc').url).to.equal('/the-component/2.0/')
    expect(version.pages.find((it) => it.path === 'the-page.adoc').url).to.equal('/the-component/2.0/the-page/')
    expect(validate(actualData)).to.be.true()
  })

  it('should add entry for alias using identity of target page in component version entry', () => {
    const contentCatalog = new ContentCatalog()
    contentCatalog.registerComponentVersion('the-component', '2.0')
    const targetPage = contentCatalog.addFile({
      src: { component: 'the-component', version: '2.0', module: 'ROOT', family: 'page', relative: 'the-page.adoc' },
      title: 'The Page',
    })
    contentCatalog.registerPageAlias('the-alias.adoc', targetPage)
    const actual = exportSiteManifest(contentCatalog, 'https://docs.example.org')
    const actualData = JSON.parse(actual.contents)
    const pages = actualData.components['the-component'].versions['2.0'].pages
    expect(pages).to.have.lengthOf(2)
    const alias = pages.find(({ path }) => path === 'the-alias.adoc')
    expect(alias).to.eql({
      path: 'the-alias.adoc',
      url: '/the-component/2.0/the-alias.html',
      title: 'The Page',
      alias: { ref: 'the-page.adoc', url: '/the-component/2.0/the-page.html' },
    })
    expect(validate(actualData)).to.be.true()
  })

  it('should add entry for every alias in component version entry', () => {
    const contentCatalog = new ContentCatalog()
    contentCatalog.registerComponentVersion('the-component', '2.0')
    const targetPage = contentCatalog.addFile({
      src: { component: 'the-component', version: '2.0', module: 'ROOT', family: 'page', relative: 'the-page.adoc' },
      title: 'The Page',
    })
    contentCatalog.registerPageAlias('the-alias.adoc', targetPage)
    contentCatalog.registerPageAlias('another-module:another-alias.adoc', targetPage)
    const actual = exportSiteManifest(contentCatalog, 'https://docs.example.org')
    const actualData = JSON.parse(actual.contents)
    const pages = actualData.components['the-component'].versions['2.0'].pages
    expect(pages).to.have.lengthOf(3)
    const theAlias = pages.find((it) => it.path === 'the-alias.adoc')
    expect(theAlias).to.eql({
      path: 'the-alias.adoc',
      title: 'The Page',
      url: '/the-component/2.0/the-alias.html',
      alias: { ref: 'the-page.adoc', url: '/the-component/2.0/the-page.html' },
    })
    const anotherAlias = pages.find((it) => it.path === 'another-alias.adoc')
    expect(anotherAlias).to.eql({
      module: 'another-module',
      path: 'another-alias.adoc',
      url: '/the-component/2.0/another-module/another-alias.html',
      title: 'The Page',
      alias: { ref: 'ROOT:the-page.adoc', url: '/the-component/2.0/the-page.html' },
    })
    expect(validate(actualData)).to.be.true()
  })

  // TODO this test is doing too much; split it up
  it('should add entry for aliases that point outside component version in component version entry', () => {
    const contentCatalog = new ContentCatalog()
    contentCatalog.registerComponentVersion('the-component', '')
    contentCatalog.registerComponentVersion('the-component', '1.0')
    contentCatalog.registerComponentVersion('other-component', '2.0')
    const targetPage = contentCatalog.addFile({
      src: { component: 'the-component', version: '1.0', module: 'ROOT', family: 'page', relative: 'the-page.adoc' },
      title: 'The Page',
    })
    const otherTargetPage = contentCatalog.addFile({
      src: { component: 'the-component', version: '1.0', module: 'start', family: 'page', relative: 'install.adoc' },
      title: 'Install Page',
    })
    const anotherTargetPage = contentCatalog.addFile({
      src: { component: 'the-component', version: '', module: 'ROOT', family: 'page', relative: 'index.adoc' },
      title: 'Home Page',
    })
    contentCatalog.registerPageAlias('_@the-page.adoc', targetPage)
    contentCatalog.registerPageAlias('2.0@other-component::the-alias.adoc', targetPage)
    contentCatalog.registerPageAlias('2.0@other-component:start:install.adoc', otherTargetPage)
    contentCatalog.registerPageAlias('2.0@other-component::index.adoc', anotherTargetPage)
    const actual = exportSiteManifest(contentCatalog, 'https://docs.example.org')
    const actualData = JSON.parse(actual.contents)
    let aliases = actualData.components['other-component'].versions['2.0'].pages.filter(({ alias }) => alias)
    expect(aliases).to.have.lengthOf(3)
    expect(aliases[0]).to.eql({
      path: 'index.adoc',
      url: '/other-component/2.0/index.html',
      title: 'Home Page',
      alias: { ref: '_@the-component::index.adoc', url: '/the-component/index.html' },
    })
    expect(aliases[1]).to.eql({
      path: 'the-alias.adoc',
      url: '/other-component/2.0/the-alias.html',
      title: 'The Page',
      alias: { ref: '1.0@the-component::the-page.adoc', url: '/the-component/1.0/the-page.html' },
    })
    expect(aliases[2]).to.eql({
      module: 'start',
      path: 'install.adoc',
      url: '/other-component/2.0/start/install.html',
      title: 'Install Page',
      alias: { ref: '1.0@the-component:start:install.adoc', url: '/the-component/1.0/start/install.html' },
    })
    aliases = actualData.components['the-component'].versions[''].pages.filter(({ alias }) => alias)
    expect(aliases).to.have.lengthOf(1)
    expect(aliases[0]).to.eql({
      path: 'the-page.adoc',
      url: '/the-component/the-page.html',
      title: 'The Page',
      alias: { ref: '1.0@the-page.adoc', url: '/the-component/1.0/the-page.html' },
    })
    expect(validate(actualData)).to.be.true()
  })

  it('should not add entry for splat alias', () => {
    const contentCatalog = new ContentCatalog({
      urls: { latestVersionSegment: 'latest', latestVersionSegmentStrategy: 'redirect:to' },
    })
    contentCatalog.registerComponentVersion('the-component', '2.0')
    const targetPage = contentCatalog.addFile({
      src: { component: 'the-component', version: '2.0', module: 'ROOT', family: 'page', relative: 'the-page.adoc' },
      title: 'The Page',
    })
    contentCatalog.registerComponentVersionStartPage('the-component', '2.0')
    contentCatalog.registerPageAlias('the-alias.adoc', targetPage)
    const actual = exportSiteManifest(contentCatalog, 'https://docs.example.org')
    const actualData = JSON.parse(actual.contents)
    const pages = actualData.components['the-component'].versions['2.0'].pages
    const aliases = pages.filter(({ alias }) => alias)
    expect(aliases).to.have.lengthOf(1)
    expect(aliases[0]).to.eql({
      path: 'the-alias.adoc',
      url: '/the-component/latest/the-alias.html',
      title: 'The Page',
      alias: { ref: 'the-page.adoc', url: '/the-component/latest/the-page.html' },
    })
  })

  it('should skip page that was imported from another site (hybrid component)', () => {
    const primarySite = { url: 'https://docs.example.org' }
    const contentCatalog = new ContentCatalog()
    contentCatalog.registerComponentVersion('home', '').hybrid = true
    contentCatalog.addFile({
      src: { component: 'home', version: '', module: 'ROOT', family: 'page', relative: 'index.adoc' },
    })
    contentCatalog.addFile({
      src: { component: 'home', version: '', module: 'ROOT', family: 'page', relative: 'imported-page.adoc' },
      site: primarySite,
    })
    const actual = exportSiteManifest(contentCatalog, primarySite.url)
    const actualData = JSON.parse(actual.contents)
    expect(Object.keys(actualData.components)).to.have.lengthOf(1)
    expect(Object.keys(actualData.components)[0]).to.equal('home')
    expect(Object.keys(actualData.components.home.versions)).to.have.lengthOf(1)
    expect(actualData.components.home.versions[''].pages).to.have.lengthOf(1)
    expect(actualData.components.home.versions[''].pages[0].path).to.equal('index.adoc')
    expect(validate(actualData)).to.be.true()
  })

  it('should sort page entries by module then by path, promoting index page to top', () => {
    const contentCatalog = new ContentCatalog()
    contentCatalog.registerComponentVersion('the-component', '2.0', { startPage: true })
    contentCatalog.addFile({
      src: { component: 'the-component', version: '2.0', module: 'ROOT', family: 'page', relative: 'about.adoc' },
      title: 'About Page',
    })
    contentCatalog.addFile({
      src: { component: 'the-component', version: '2.0', module: 'b', family: 'page', relative: 'index.adoc' },
      title: 'B',
    })
    const lastPage = contentCatalog.addFile({
      src: { component: 'the-component', version: '2.0', module: 'a', family: 'page', relative: 'z-page.adoc' },
      title: 'Last Page',
    })
    contentCatalog.addFile({
      src: { component: 'the-component', version: '2.0', module: 'a', family: 'page', relative: 'a-page.adoc' },
      title: 'The First Page',
      asciidoc: { xreftext: 'First Page' },
    })
    contentCatalog.addFile({
      src: { component: 'the-component', version: '2.0', module: 'ROOT', family: 'page', relative: 'index.adoc' },
      title: 'Index Page',
    })
    contentCatalog.addFile({
      src: { component: 'the-component', version: '2.0', module: 'a', family: 'page', relative: 'index.adoc' },
      title: 'Module A',
    })
    contentCatalog.registerPageAlias('last-page.adoc', lastPage)
    const actual = exportSiteManifest(contentCatalog, 'https://docs.example.org')
    const actualData = JSON.parse(actual.contents)
    const pages = actualData.components['the-component'].versions['2.0'].pages
    expect(pages).to.have.lengthOf(7)
    expect(pages[0]).to.eql({
      path: 'index.adoc',
      url: '/the-component/2.0/index.html',
      title: 'Index Page',
    })
    expect(pages[1]).to.eql({
      path: 'about.adoc',
      url: '/the-component/2.0/about.html',
      title: 'About Page',
    })
    expect(pages[2]).to.eql({
      module: 'a',
      path: 'index.adoc',
      url: '/the-component/2.0/a/index.html',
      title: 'Module A',
    })
    expect(pages[3]).to.eql({
      module: 'a',
      path: 'a-page.adoc',
      url: '/the-component/2.0/a/a-page.html',
      title: 'The First Page',
      xreftext: 'First Page',
    })
    expect(pages[4]).to.eql({
      module: 'a',
      path: 'last-page.adoc',
      url: '/the-component/2.0/a/last-page.html',
      title: 'Last Page',
      alias: { ref: 'z-page.adoc', url: '/the-component/2.0/a/z-page.html' },
    })
    expect(pages[5]).to.eql({
      module: 'a',
      path: 'z-page.adoc',
      url: '/the-component/2.0/a/z-page.html',
      title: 'Last Page',
    })
    expect(pages[6]).to.eql({
      module: 'b',
      path: 'index.adoc',
      url: '/the-component/2.0/b/index.html',
      title: 'B',
    })
    expect(validate(actualData)).to.be.true()
  })
})

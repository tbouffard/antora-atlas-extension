/* eslint-env mocha */
'use strict'

const ContentCatalog = require('@antora/content-classifier/content-catalog')
const { expect, spy } = require('./harness')
const ospath = require('path')
const { isProxy } = require('node:util').types

const FIXTURES_DIR = ospath.join(__dirname, 'fixtures')

describe('extension', () => {
  const ext = require('@antora/atlas-extension')

  const createGeneratorContext = () => ({
    functions: { mapSite: () => undefined },
    variables: {},
    getFunctions () {
      return Object.assign({}, this.functions)
    },
    once (eventName, fn) {
      this[eventName] = fn
    },
    replaceFunctions (updates) {
      Object.assign(this.functions, updates)
    },
    require,
    updateVariables (updates) {
      Object.assign(this.variables, updates)
    },
  })

  let generatorContext
  let playbook
  let siteAsciiDocConfig
  let contentCatalog

  beforeEach(() => {
    generatorContext = createGeneratorContext()
    playbook = { dir: FIXTURES_DIR }
    siteAsciiDocConfig = { attributes: {} }
    contentCatalog = new ContentCatalog()
  })

  describe('bootstrap', () => {
    it('should be able to require extension', () => {
      expect(ext).to.be.instanceOf(Object)
      expect(ext.register).to.be.instanceOf(Function)
    })

    it('should be able to call register function exported by extension', () => {
      ext.register.call(generatorContext)
      expect(generatorContext.componentsRegistered).to.be.instanceOf(Function)
      expect(generatorContext.beforePublish).to.be.instanceOf(Function)
    })
  })

  describe('import (componentsRegistered listener)', () => {
    const addFilesAndRegisterStartPages = (contentCatalog) => {
      contentCatalog.getComponents().forEach(({ versions: componentVersions }) => {
        componentVersions.forEach((componentVersion) => {
          const { name: component, files = [], startPage } = componentVersion
          for (let file, iter = files.reverse(); (file = iter.pop());) contentCatalog.addFile(file, componentVersion)
          contentCatalog.registerComponentVersionStartPage(component, componentVersion, startPage)
        })
      })
    }

    it('should not import manifest if neither primary site URL or primary site manifest URL are specified', () => {
      ext.register.call(generatorContext)
      generatorContext.updateVariables({ siteAsciiDocConfig })
      generatorContext.updateVariables = spy(generatorContext.updateVariables)
      generatorContext.require = spy(generatorContext.require)
      const returnValue = generatorContext.componentsRegistered(generatorContext.variables)
      expect(returnValue).to.be.undefined()
      expect(generatorContext.updateVariables).to.not.have.been.called()
      expect(generatorContext.require).to.not.have.been.called()
    })

    it('should not proxy content catalog if nothing is imported', async () => {
      siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './own-site-manifest.json'
      ext.register.call(generatorContext)
      generatorContext.require = spy(generatorContext.require)
      contentCatalog.registerComponentVersion('the-component', '1.0', { startPage: true })
      generatorContext.updateVariables({ playbook, siteAsciiDocConfig, contentCatalog })
      await generatorContext.componentsRegistered(generatorContext.variables)
      expect(contentCatalog.getComponents()).to.have.lengthOf(1)
      expect(isProxy(generatorContext.variables.contentCatalog)).to.be.false()
      expect(generatorContext.require).to.have.been.called.with('@antora/content-classifier/util/parse-resource-id')
    })

    it('should import manifest if primary site manifest URL is specified', async () => {
      siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './new-component-site-manifest.json'
      ext.register.call(generatorContext)
      generatorContext.updateVariables({ playbook, siteAsciiDocConfig, contentCatalog })
      const returnValue = generatorContext.componentsRegistered(generatorContext.variables)
      expect(returnValue).to.be.instanceOf(Promise)
      expect(await returnValue).to.be.undefined()
      expect(isProxy(generatorContext.variables.contentCatalog)).to.be.true()
      expect(contentCatalog.getComponents()).to.have.lengthOf(1)
    })

    it('should proxy mapSite to use all publishable pages in same site mode', async () => {
      siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './new-component-site-manifest.json'
      siteAsciiDocConfig.attributes['primary-site-url'] = '.'
      generatorContext.replaceFunctions({ mapSite: (_, pages) => pages })
      ext.register.call(generatorContext)
      Object.assign(contentCatalog.registerComponentVersion('the-component', '1.0', { title: 'The Component' }), {
        files: [
          {
            path: 'modules/ROOT/pages/index.adoc',
            src: { component: 'the-component', version: '1.0', module: 'ROOT', family: 'page', relative: 'index.adoc' },
          },
        ],
      })
      generatorContext.updateVariables({ playbook, siteAsciiDocConfig, contentCatalog })
      await generatorContext.componentsRegistered(generatorContext.variables)
      contentCatalog = generatorContext.variables.contentCatalog
      addFilesAndRegisterStartPages(contentCatalog)
      expect(contentCatalog.getComponents()).to.have.lengthOf(2)
      const pages = contentCatalog.getPages()
      expect(pages).to.have.lengthOf(3)
      const publishablePages = pages.filter(({ out }) => out)
      expect(publishablePages).to.have.lengthOf(1)
      const { mapSite } = generatorContext.getFunctions()
      expect(isProxy(mapSite)).to.be.true()
      const pagesInSitemap = mapSite(playbook, publishablePages)
      expect(pagesInSitemap).to.have.lengthOf(3)
      const componentsInSitemap = [...new Set(pagesInSitemap.map(({ src }) => src.component))]
      expect(componentsInSitemap).to.have.members(['new-component', 'the-component'])
    })

    it('should not proxy mapSite in sharded mode', async () => {
      siteAsciiDocConfig.attributes['primary-site-manifest-url'] = './new-component-site-manifest.json'
      generatorContext.replaceFunctions({ mapSite: (_, pages) => pages })
      ext.register.call(generatorContext)
      Object.assign(contentCatalog.registerComponentVersion('the-component', '1.0', { title: 'The Component' }), {
        files: [
          {
            path: 'modules/ROOT/pages/index.adoc',
            src: { component: 'the-component', version: '1.0', module: 'ROOT', family: 'page', relative: 'index.adoc' },
          },
        ],
      })
      generatorContext.updateVariables({ playbook, siteAsciiDocConfig, contentCatalog })
      await generatorContext.componentsRegistered(generatorContext.variables)
      contentCatalog = generatorContext.variables.contentCatalog
      addFilesAndRegisterStartPages(contentCatalog)
      expect(contentCatalog.getComponents()).to.have.lengthOf(2)
      const pages = contentCatalog.getPages()
      expect(pages).to.have.lengthOf(3)
      const publishablePages = pages.filter(({ out }) => out)
      expect(publishablePages).to.have.lengthOf(1)
      const { mapSite } = generatorContext.getFunctions()
      expect(isProxy(mapSite)).to.be.false()
      const pagesInSitemap = mapSite(playbook, publishablePages)
      expect(pagesInSitemap).to.have.lengthOf(1)
    })
  })

  describe('export (beforePublish listener)', () => {
    it('should export to manifest to site file named site-manifest.json', () => {
      playbook.site = { url: 'https://docs.example.org' }
      const siteCatalog = {
        files: [],
        addFile (file) {
          this.files.push(file)
        },
      }
      contentCatalog.registerComponentVersion('the-component', '1.0')
      contentCatalog.addFile({
        src: { component: 'the-component', version: '1.0', module: 'ROOT', family: 'page', relative: 'index.adoc' },
      })
      contentCatalog.addFile({
        src: { component: 'the-component', version: '1.0', module: 'ROOT', family: 'page', relative: 'the-page.adoc' },
      })
      contentCatalog.registerComponentVersionStartPage('the-component', '1.0')
      ext.register.call(generatorContext)
      generatorContext.updateVariables({ playbook, siteAsciiDocConfig, contentCatalog, siteCatalog })
      generatorContext.beforePublish(generatorContext.variables)
      expect(siteCatalog.files).to.have.lengthOf(1)
      const siteManifestFile = siteCatalog.files[0]
      expect(siteManifestFile.path).to.equal('site-manifest.json')
      const siteManifest = JSON.parse(siteManifestFile.contents)
      expect(siteManifest.url).to.eql('https://docs.example.org')
      expect(Object.keys(siteManifest.components)).to.have.lengthOf(1)
      expect(Object.keys(siteManifest.components['the-component'].versions)).to.have.lengthOf(1)
      expect(siteManifest.components['the-component'].versions['1.0'].pages).to.have.lengthOf(2)
    })

    it('should allow manifest file path to be changed using site-manifest-path attribute', () => {
      playbook.site = { url: 'https://docs.example.org' }
      siteAsciiDocConfig.attributes['site-manifest-path'] = 'manifest.json'
      const siteCatalog = {
        files: [],
        addFile (file) {
          this.files.push(file)
        },
      }
      contentCatalog.registerComponentVersion('the-component', '1.0')
      contentCatalog.addFile({
        src: { component: 'the-component', version: '1.0', module: 'ROOT', family: 'page', relative: 'index.adoc' },
      })
      contentCatalog.registerComponentVersionStartPage('the-component', '1.0')
      ext.register.call(generatorContext)
      generatorContext.updateVariables({ playbook, siteAsciiDocConfig, contentCatalog, siteCatalog })
      generatorContext.beforePublish(generatorContext.variables)
      expect(siteCatalog.files).to.have.lengthOf(1)
      const siteManifestFile = siteCatalog.files[0]
      expect(siteManifestFile.path).to.equal('manifest.json')
    })

    it('should include all aliases and publishable pages in export', () => {
      ext.register.call(generatorContext)
      playbook.site = { url: 'https://docs.example.org' }
      const siteCatalog = {
        files: [],
        addFile (file) {
          this.files.push(file)
        },
      }
      contentCatalog.registerComponentVersion('the-component', '1.0')
      contentCatalog.addFile({
        src: { component: 'the-component', version: '1.0', module: 'ROOT', family: 'partial', relative: 'intro.adoc' },
      })
      contentCatalog.addFile({
        src: { component: 'the-component', version: '1.0', module: 'ROOT', family: 'example', relative: 'hello.rb' },
      })
      const thePage = contentCatalog.addFile({
        src: { component: 'the-component', version: '1.0', module: 'ROOT', family: 'page', relative: 'the-page.adoc' },
      })
      contentCatalog.registerComponentVersionStartPage('the-component', '1.0', 'the-page.adoc')
      contentCatalog.registerPageAlias('the-alias.adoc', thePage)
      generatorContext.updateVariables({ playbook, siteAsciiDocConfig, contentCatalog, siteCatalog })
      generatorContext.beforePublish(generatorContext.variables)
      expect(siteCatalog.files).to.have.lengthOf(1)
      const siteManifestFile = siteCatalog.files[0]
      expect(siteManifestFile.path).to.equal('site-manifest.json')
      const siteManifest = JSON.parse(siteManifestFile.contents)
      expect(contentCatalog.getFiles()).to.have.lengthOf(5)
      const pages = siteManifest.components['the-component'].versions['1.0'].pages
      expect(pages).to.have.lengthOf(3)
      expect(pages.filter((it) => it.alias)).to.have.lengthOf(2)
    })
  })
})

'use strict'

const readSiteManifest = require('./read-site-manifest')

async function importSiteManifest (playbook, asciidoc, contentCatalog, parseResourceRef) {
  const siteUrl = playbook.site?.url
  let { 'primary-site-manifest-url': primarySiteManifestUrl, 'primary-site-url': primarySiteUrl } = asciidoc.attributes
  primarySiteManifestUrl ||= ((primarySiteUrl === '.' ? siteUrl : primarySiteUrl) || '.') + '/site-manifest.json'
  const { components = {}, url: siteUrlFromManifest } = await readSiteManifest(playbook, primarySiteManifestUrl)
  if (!Object.keys(components).length) return
  primarySiteUrl ||= siteUrlFromManifest
  let primarySite
  if (primarySiteUrl === '.' || siteUrl === primarySiteUrl) {
    primarySiteUrl = undefined
  } else {
    primarySite = { url: primarySiteUrl }
    Object.assign(asciidoc.attributes, {
      'primary-site-url': primarySiteUrl,
      'primary-site-manifest-url': primarySiteManifestUrl,
    })
  }
  return Object.entries(components).reduce((imported, [componentName, { title, versions = {} }]) => {
    if (!Object.keys(versions).length) return imported
    let component = contentCatalog.getComponent(componentName)
    Object.entries(versions).forEach(([versionName, { prerelease, displayVersion, pages, url: startUrl }]) => {
      let componentVersion, files, hybrid
      if (!(componentVersion = component && contentCatalog.getComponentVersion(component, versionName))) {
        const componentVersionData = { prerelease, displayVersion, title, asciidoc }
        componentVersion = Object.assign(
          contentCatalog.registerComponentVersion(componentName, versionName, componentVersionData),
          { files: (files = []) },
          primarySite ? { site: primarySite, url: primarySiteUrl + startUrl } : { url: startUrl }
        )
        component ??= Object.assign(
          contentCatalog.getComponent(componentName),
          primarySite ? { site: primarySite } : undefined
        )
        imported = primarySite || {}
      } else if (componentName === 'home' && !versionName) {
        componentVersion.hybrid = hybrid = true
        files = componentVersion.files ||= []
      } else {
        return imported // NOTE skip component version if already in this site (unless component is home)
      }
      pages.forEach(({ module = 'ROOT', path: relative, title: doctitle = 'Untitled', xreftext, url, alias }) => {
        const srcPath = ['modules', module, 'pages', relative].join('/')
        if (hybrid && files.find((candidate) => candidate.path === srcPath)) return // NOTE don't overwrite local page
        const family = 'page'
        const src = { component: componentName, version: versionName, module, relative, family, extname: '.adoc' }
        let asciidoc = { doctitle, xreftext: xreftext ?? doctitle }
        let rel
        if (primarySite) {
          url = primarySiteUrl + (alias ? alias.url : url)
        } else if (alias) {
          if (alias.url === startUrl && module === 'ROOT' && relative === 'index.adoc') return
          rel = { asciidoc, src: parseResourceRef(alias.ref, src), pub: { url: alias.url }, title: doctitle }
          src.family = 'alias'
          asciidoc = doctitle = undefined
        } else if (url === startUrl) {
          componentVersion.startPage ??= module + ':' + relative
        }
        const site = primarySite
        files.push({ asciidoc, out: undefined, path: srcPath, pub: { url }, rel, site, src, title: doctitle })
      })
    })
    return imported
  }, undefined)
}

module.exports = importSiteManifest

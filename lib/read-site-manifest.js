'use strict'

const expandPath = require('@antora/expand-path-helper')
const { promises: fsp } = require('fs')
const { concat: get } = require('simple-get')
const getCacheDir = require('cache-directory')
const { ungzip: gunzip } = require('node-gzip')
const ospath = require('path')

const URI_SCHEME_RX = /^https?:\/\//

async function readSiteManifest (playbook, manifestUrl) {
  const manifestPath = await (isUrl(manifestUrl)
    ? downloadSiteManifest(playbook, manifestUrl)
    : expandPath(manifestUrl, { dot: playbook.dir }))
  try {
    return JSON.parse(
      await fsp.readFile(manifestPath).then((contents) => (manifestPath.endsWith('.gz') ? gunzip(contents) : contents))
    )
  } catch {
    return {}
  }
}

function isUrl (string) {
  return ~string.indexOf('://') && URI_SCHEME_RX.test(string)
}

function downloadSiteManifest (playbook, url) {
  const { cacheDir, fetch } = playbook.runtime || {}
  const resolvedCacheDir = resolveCacheDir(cacheDir, playbook.dir || '.')
  const gzipped = url.endsWith('.gz')
  const cacheFile = ospath.join(resolvedCacheDir, `${new URL(url).hostname}-site-manifest.json${gzipped ? '.gz' : ''}`)
  return (
    fetch
      ? Promise.resolve(false)
      : fsp.access(cacheFile).then(
        () => true,
        () => false
      )
  ).then((exists) =>
    exists
      ? cacheFile
      : fsp.mkdir(resolvedCacheDir, { recursive: true }).then(
        () =>
          new Promise((resolve, reject) =>
            get({ url }, (err, response, contents) => {
              if (err) reject(err)
              if (response.statusCode !== 200) {
                const message = `Response code ${response.statusCode} (${response.statusMessage})`
                return reject(Object.assign(new Error(message), { name: 'HTTPError' }))
              }
              return fsp.writeFile(cacheFile, contents).then(() => resolve(cacheFile))
            })
          )
      )
  )
}

function resolveCacheDir (cacheDir, startDir) {
  return cacheDir == null
    ? getCacheDir('antora') || ospath.resolve('.antora/cache')
    : expandPath(cacheDir, { dot: startDir })
}

module.exports = readSiteManifest
